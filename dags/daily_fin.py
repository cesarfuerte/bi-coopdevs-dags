from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.models import Variable

from datetime import datetime

from odoofin import odoofin


def calcula_fin():
    odoofin.calculaodoofin();


with DAG("Daily_Fin_Odoo", start_date=datetime(2021, 1, 1), schedule_interval="0 6 * * *", catchup=False) as dag:

    calcula_fin = PythonOperator(
        task_id="incremental",
        python_callable=calcula_fin,
        email_on_failure=True,
        email=Variable.get("mail_zulip")
    )

    calcula_fin

